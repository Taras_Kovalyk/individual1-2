﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Individual1.Task2.Core
{
    public struct Point
    {
        public Point(double x, double y)
        {
            X = x;
            Y = y;
        }

        public double X
        {
            get;
        }

        public double Y
        {
            get;
        }

        public override string ToString()
        {
            return String.Format("({0};{1})", X, Y);
        }

    }

}
