﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Individual1.Task1.Core
{
    public class NumberVector : IEnumerable
    {
        private int[] _data;
        private int _indexOffset;

        public NumberVector(int size) : this(0, size)
        { }

        public NumberVector(int startIndex, int size)
        {
            _indexOffset = startIndex;
            _data = new int[size];
        }

        private void AssertCorrectIndex(int index)
        {
            if (ConvertIndex(index) < 0 || ConvertIndex(index) >= _data.Length)
            {
                throw new IndexOutOfRangeException("Index is out of range");
            }
        }

        public int StartIndex
        {
            get
            {
                return _indexOffset;
            }
        }

        internal int[] Data
        {
            get
            {
                return _data;
            }
        }

        public int Size
        {
            get
            {
                return _data.Length;
            }
        }

        public void Multiply(int number)
        {
            for (int i = 0; i < _data.Length; i++)
            {
                _data[i] *= number;
            }
        }

        private int ConvertIndex(int index)
        {
            return index - _indexOffset;
        }

        public int this[int index]
        {
            get
            {
                AssertCorrectIndex(index);

                return _data[ConvertIndex(index)];
            }

            set
            {
                AssertCorrectIndex(index);

                _data[ConvertIndex(index)] = value;
            }
        }

        public static NumberVector operator+(NumberVector v1, NumberVector v2)
        {
            if (v1.Size != v2.Size || v1.StartIndex != v2.StartIndex)
            {
                throw new DifferentSizeException("Can't add two vectors, the size or range is different");
            }

            NumberVector newVector = new NumberVector(v1.Size);

            for (int i = 0; i < v1.Size; i++)
            {
                newVector[i] = v1.Data[i] + v2.Data[i];
            }

            return newVector;
        }

        public static NumberVector operator-(NumberVector v1, NumberVector v2)
        {
            if (v1.Size != v2.Size || v1.StartIndex != v2.StartIndex)
            {
                throw new DifferentSizeException("Can't subtract two vectors, the size or range is different");
            }

            NumberVector newVector = new NumberVector(v1.Size);

            for (int i = 0; i < v1.Size; i++)
            {
                newVector[i] = v1.Data[i] - v2.Data[i];
            }

            return newVector;
        }

        public static bool operator==(NumberVector v1, NumberVector v2)
        {
            return v1.Equals(v2);
        }

        public static bool operator!=(NumberVector v1, NumberVector v2)
        {
            return !v1.Equals(v2);
        }

        public override bool Equals(object obj)
        {
            if (!(obj is NumberVector))
                return false;

            NumberVector vector = (NumberVector) obj;

            if (vector.Size != Size)
            {
                return false;
            }

            return Data.SequenceEqual(vector.Data);
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }

        public IEnumerator GetEnumerator()
        {
            return _data.GetEnumerator();
        }

        public override string ToString()
        {
            StringBuilder builder = new StringBuilder();

            foreach (int number in _data)
            {
                builder.Append(number + "   ");
            }

            return builder.ToString();
        }
    }
}
